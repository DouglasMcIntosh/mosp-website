<!DOCTYPE html>

<html>
<head>
  <title>Napier Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/styleForm.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
</head>

	
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a style="color:#bf0a0a;" class="navbar-brand" href="#">Napier Management System</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="#"><a href="#">Home</a></li>
      <li><a href="#">Course</a></li>
      <li><a href="#">Registration</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>
</nav>
<body>
<!-- your html form -->
<div class="container">
<form method="POST">
	<div class="form-group">
	<label for="username">Username</label>
	<input type="text" class="form-control" name="uname">
	<br />
	</div>
	<div class="form-group">
	<label for="password">Password</label>
	<input type="password" class="form-control" name="pass">
	<br />
	</div class>
	<div style="text-align: center;">
  <button style="background-color:#bf0a0a;"" type="submit"  class="btn btn-danger btn-lg">Submit</button>
  </div>
</form>

</div>
 
<?php 
	// this will trigger when submit button click
	if(isset($_POST['sub'])){
 
		$db = new mysqli("localhost","root","password","nms");
 
		// create query
		$query = "SELECT * FROM tbl_test WHERE username='".$_POST['uname']."' AND password='".$_POST['pass']."'";
 
		// execute query
		$sql = $db->query($query);
		// num_rows will count the affected rows base on your sql query. so $n will return a number base on your query
		$n = $sql->num_rows;
 
		// if $n is > 0 it mean their is an existing record that match base on your query above 
		if($n > 0){
 
			echo "Logged in correctly";
		} else {
 
			echo "Incorrect username or password";
		}
	}
?>
</body>
</html>