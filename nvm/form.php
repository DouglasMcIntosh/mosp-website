<?php
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "nms";

$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
echo "Connected successfully";



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Napier Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/styleForm.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
</head>
<body>
	<script type="text/javascript">
	
	

	</script>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a style="color:#bf0a0a;" class="navbar-brand" href="#">Napier Management System</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="#"><a href="#">Home</a></li>
      <li><a href="#">Course</a></li>
      <li><a href="#">Registration</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>
</nav>
<div class="container">
	<h1>Course Selection</h1>
	<form  id="myForm"  action="form.html" method="post">
	<div class="form-group">
		<label for="nameinput">First Name</label>
		<input type="text" class="form-control" id="nameInput" placeholder="Enter first name" required>
	</div>
	<div class="form-group">
		<label for="secondNameInput">Second Name</label>
		<input type="text" class="form-control" id="secondNameInput" placeholder="Enter Second name" required>
	</div>
	<div class="form-group">
		<label for="StreetNameInput">Street Name</label>
		<input type="text" class="form-control" id="streetInput" placeholder="Enter Your Street Name - eg falcon avenue" required>
	</div>
	<div class="form-group">
		<label for="PostCodeInput">Postcode</label>
		<input type="text" class="form-control" id="postCodeInput" placeholder="Enter your Postcode - eg  EH16 9PB" required>
	</div>
	<div class="form-group">
		<label for="FormControlSelectLocation">Select location</label>
    <select class="form-control" id="FormControlSelectLocation">
      <option>Edinburgh</option>
      <option>Glasgow</option>
      <option>Aberdeen</option>
    </select>
	</div>
	<div class="form-group">
		<label for="FormControlSelectCourse">Select Course</label>
    <select class="form-control" id="FormControlSelectCourse">
      <option>Prince2</option>
      <option>Programme Management</option>
      <option> Management of Risk</option>
	  <option>ITIL</option>
	  <option> Change Management</option>
    </select>
	</div>
  <div class="form-group">
    <label for="InputEmail">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter email"  required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword">Password</label>
    <input type="text" class="form-control" id="exampleInputPassword" placeholder="Password"  required>
  </div>
  <div style="text-align: center;">
  <button style="background-color:#bf0a0a;"" type="submit"  class="btn btn-danger btn-lg">Sign up</button>
  </div>
	</form>
</div>
</body>
</html>