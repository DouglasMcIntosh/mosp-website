
<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Napier Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/styleForm.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
</head>
<body>
	<script type="text/javascript">
	
	

	</script>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a style="color:#bf0a0a;" class="navbar-brand" href="#">Napier Management System</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="#"><a href="#">Home</a></li>
      <li><a href="#">Course</a></li>
      <li><a href="#">Registration</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>
</nav>

<div class="container">
<?PHP
		$servername = "localhost";
	$username = "root";
	$password = "password";
	$dbname = "nms";

	// Create connection
	$conn = new mysqli($servername, $username, $password,$dbname);

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	$sql = "SELECT eventName, eventDate,  eventTime, eventCost FROM event";
	$result = $conn->query($sql);

	

	
	echo "<table class='table'>
	<tr>
	<th>Name</th>
	<th>Date</th>
	<th>Time</th>
	<th>Cost</th>
	</tr>";
	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
		echo "<tr>";
		echo "<td>" . $row['eventName'] . "</td>";
		echo "<td>" . $row['eventDate'] . "</td>";
		echo "<td>" . $row['eventTime'] . "</td>";
		echo "<td>" . $row['eventCost'] . "</td>";
		
		echo "</tr>";
		
		}
	} else {
		echo "0 results";
	}
	
	
	echo "</table>";
	

	$conn->close();
?>

	<div style="text-align: center;">
  <button style="background-color:#bf0a0a;"" type="submit"  class="btn btn-danger btn-lg">Set Course</button>
  </div>
</form>

</div>

</body>
</html>
