<?php
$servername = "localhost";
$username = "root";
$password = "password";
$dbname = "nms";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 



$sql = "SELECT firstName, secondName, postCode, streetName, email, password FROM simple WHERE id = 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
         $fname = $row["firstName"];  "" . $sname = $row["secondName"]; " " . $streetName =$row["streetName"];  " " . $email = $row["email"];  " " . $password= $row["password"];  " " .  $pcode = $row["postCode"];  "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Napier Management System</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/styleSheetForm.css">
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  
</head>
<body>
	<script type="text/javascript">
	
	

	</script>


<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a style="color:#bf0a0a;" class="navbar-brand" href="#">Napier Management System</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="#"><a href="#">Home</a></li>
      <li><a href="#">Course</a></li>
      <li><a href="#">Registration</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div>
</nav>
	<div class="container">
	<h1>Course Registration</h1>
	<form  id="myForm"  action="form.html" method="post">
	<div class="form-group">
		<label for="nameinput">First Name</label>
		<h3><?PHP echo $fname ?></h3>
	</div>
	<div class="form-group">
		<label for="secondNameInput">Second Name</label>
		<h3><?PHP echo $sname ?></h3>
	</div>
	<div class="form-group">
		<label for="StreetNameInput">Street Name</label>
		<h3><?PHP echo $streetName ?></h3>
	</div>
	<div class="form-group">
		<label for="PostCodeInput">Postcode</label>
		<h3><?PHP echo $pcode ?></h3>
	</div>
	<div class="form-group">
		<label for="FormControlSelectLocation">Select location</label>
    <select class="form-control" id="FormControlSelectLocation">
      <option>Edinburgh</option>
      <option>Glasgow</option>
      <option>Aberdeen</option>
    </select>
	</div>
	<div class="form-group">
		<label for="FormControlSelectCourse">Select Course</label>
    <select class="form-control" id="FormControlSelectCourse">
      <option>Prince2</option>
      <option>Programme Management</option>
      <option> Management of Risk</option>
	  <option>ITIL</option>
	  <option> Change Management</option>
    </select>
	</div>
  <div class="form-group">
    <label for="InputEmail">Email address</label>
    <h3><?PHP echo $email ?></h3>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword">Password</label>
   <h3><?PHP echo $password  ?></h3>
  </div>
  <div style="text-align: center;">
  <button style="background-color:#bf0a0a;"" type="submit"  class="btn btn-danger btn-lg">Sign up</button>
  </div>
	</form>
</div>

</body>
</html>